package com.transpire.abc.abcnews.data.net

import com.transpire.abc.abcnews.data.DataStored
import com.transpire.abc.abcnews.domain.models.NewsArticles
import io.reactivex.Observable

import retrofit2.Retrofit

/**
 * Restservice call the remote api using retrofit
 */

class RestService (private val retrofit: Retrofit) : DataStored {

    private val relativeUrl: String = "v1/api.json?rss_url=http://www.abc.net.au/news/feed/51120/rss.xml"

    override fun obtainNewsFeed(): Observable<NewsArticles> = retrofit.create(FeedNewsApi::class.java).feedNews(relativeUrl)

}
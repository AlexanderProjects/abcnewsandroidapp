package com.transpire.abc.abcnews.di.modules

import android.app.Application
import android.content.Context
import com.transpire.abc.abcnews.AbcNewsApplication
import com.transpire.abc.abcnews.data.DataFactory
import com.transpire.abc.abcnews.data.FeedNewsDataRepository
import com.transpire.abc.abcnews.di.factory.ViewModelFactory
import com.transpire.abc.abcnews.domain.repository.FeedNewsRepository
import com.transpire.abc.abcnews.domain.usecases.NewsUseCase
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class ApplicationModule(private val abcNewsApplication: AbcNewsApplication) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Application = abcNewsApplication

    @Provides
    @Singleton
    fun provideNewsUseCase(feedNewsDataRepository: FeedNewsDataRepository) : NewsUseCase = NewsUseCase(feedNewsDataRepository)

    @Provides
    @Singleton
    fun provideFeedNewsRepository(dataFactory: DataFactory) : FeedNewsRepository = FeedNewsDataRepository(dataFactory)

    @Provides
    @Singleton
    fun provideDataFactory(retrofit: Retrofit) : DataFactory = DataFactory(retrofit)


}
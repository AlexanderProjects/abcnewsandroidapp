package com.transpire.abc.abcnews.ui.Adapters.viewholders

import android.view.View
import com.squareup.picasso.Picasso
import com.transpire.abc.abcnews.R
import com.transpire.abc.abcnews.domain.models.Item
import kotlinx.android.synthetic.main.header_article_news_item.view.*


class HeaderViewHolder(itemView: View, private var picasso: Picasso) : BaseViewHolder(itemView)
{

    override fun bind(article: Item) = with(itemView) {
        header_news_title.text = article.title
        header_news_date.text = article.pubDate?.let { pubDateFormated(it) }
        picasso
            .load(article.enclosure.link)
            .placeholder(R.mipmap.ic_launcher_round)
            .error(R.drawable.ic_sync_black_24dp)
            .into(header_news_image)

    }
}
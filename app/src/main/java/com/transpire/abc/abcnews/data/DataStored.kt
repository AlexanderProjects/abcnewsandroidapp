package com.transpire.abc.abcnews.data

import com.transpire.abc.abcnews.domain.models.NewsArticles
import io.reactivex.Observable

interface DataStored {
    fun obtainNewsFeed(): Observable<NewsArticles>
}
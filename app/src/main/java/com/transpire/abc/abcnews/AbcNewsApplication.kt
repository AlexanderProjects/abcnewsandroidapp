package com.transpire.abc.abcnews

import android.app.Activity
import android.app.Application
import android.support.v4.app.Fragment
import com.transpire.abc.abcnews.di.components.ApplicationComponent
import com.transpire.abc.abcnews.di.components.DaggerApplicationComponent
import com.transpire.abc.abcnews.di.modules.ApplicationModule
import com.transpire.abc.abcnews.di.modules.NetModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class AbcNewsApplication : Application(), HasActivityInjector, HasSupportFragmentInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    lateinit var component : ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .netModule(NetModule(getString(R.string.AbcNewsRssUrl)))
            .build()
        component.inject(this)

    }

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector



}
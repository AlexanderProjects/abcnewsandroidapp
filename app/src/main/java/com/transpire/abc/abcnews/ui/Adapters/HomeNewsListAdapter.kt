package com.transpire.abc.abcnews.ui.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.transpire.abc.abcnews.R
import com.transpire.abc.abcnews.domain.models.Item
import com.transpire.abc.abcnews.ui.Adapters.viewholders.BaseViewHolder
import com.transpire.abc.abcnews.ui.Adapters.viewholders.HeaderViewHolder
import com.transpire.abc.abcnews.ui.Adapters.viewholders.ItemViewHolder


class HomeNewsListAdapter(private val articles : ArrayList<Item>, private var picasso: Picasso) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder{
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(viewType, parent, false)
        var holder: BaseViewHolder

         when (viewType) {
             R.layout.article_news_item -> holder = ItemViewHolder(view, picasso)
             R.layout.header_article_news_item -> holder = HeaderViewHolder(view, picasso)
             else -> holder = ItemViewHolder(view, picasso)
         }

        return holder
    }


    override fun getItemCount(): Int = articles.size

    override fun getItemViewType(position: Int): Int {
        when (position){
            0 -> return R.layout.header_article_news_item
            2 -> return R.layout.article_news_item
            else -> return R.layout.article_news_item
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder.itemViewType) {
            R.layout.article_news_item -> (holder as ItemViewHolder).bind(articles.get(holder.adapterPosition))
            R.layout.header_article_news_item -> (holder as HeaderViewHolder).bind(articles.get(holder.adapterPosition))
        }

    }




}
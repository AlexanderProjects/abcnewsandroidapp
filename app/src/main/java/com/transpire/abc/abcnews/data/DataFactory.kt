package com.transpire.abc.abcnews.data

import com.transpire.abc.abcnews.data.net.RestService
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

/**
 *  This class implement factory pattern for any resources such as restservice or databaseservice
 */

@Singleton
class DataFactory @Inject constructor(private val retrofit: Retrofit) {

    fun createSourceData(): DataStored = RestService(retrofit)

}
package com.transpire.abc.abcnews.ui.homenews

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.transpire.abc.abcnews.AbcNewsApplication
import com.transpire.abc.abcnews.R
import com.transpire.abc.abcnews.domain.models.Item
import com.transpire.abc.abcnews.ui.Adapters.HomeNewsListAdapter
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * This fragment inflate recycler view
 */

class HomeNewsFragment : Fragment(), HomeNewsView, SwipeRefreshLayout.OnRefreshListener{


    companion object {
        fun newInstance() = HomeNewsFragment()
    }

    private lateinit var viewModel: HomeNewsViewModel
    private lateinit var listOfNewsView : RecyclerView
    private lateinit var listOfNewsAdapter : HomeNewsListAdapter
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var linearLayoutManager: LinearLayoutManager

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var piccaso : Picasso


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view =  inflater.inflate(R.layout.home_news_fragment, container, false)
        listOfNewsView = view.findViewById(R.id.recycler_list_of_news)
        swipeRefreshLayout = view.findViewById(R.id.swipe_container)
        swipeRefreshLayout.setOnRefreshListener(this)
        return view
    }

    override fun setAdapter(listOfNews: ArrayList<Item>) {
        linearLayoutManager = LinearLayoutManager(activity)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        listOfNewsView.layoutManager = linearLayoutManager
        listOfNewsAdapter = HomeNewsListAdapter(listOfNews, piccaso)
        listOfNewsView.adapter = listOfNewsAdapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity?.application as AbcNewsApplication).component
            .inject(this)
        viewModel = ViewModelProviders.of(this,viewModelFactory).get(HomeNewsViewModel::class.java)
        viewModel.injectView(this)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getListOfNews()
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onRefresh() {
        swipeRefreshLayout.isRefreshing = true
        viewModel.getListOfNews()
    }

    override fun stopRefresh() {
        swipeRefreshLayout.isRefreshing = false
    }
}

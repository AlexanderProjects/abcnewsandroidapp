package com.transpire.abc.abcnews.ui.homenews

import com.transpire.abc.abcnews.domain.models.Item

interface HomeNewsView {
    fun setAdapter (listOfNews:ArrayList<Item>)
    fun stopRefresh()
}
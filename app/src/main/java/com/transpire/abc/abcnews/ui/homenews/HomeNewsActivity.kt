package com.transpire.abc.abcnews.ui.homenews

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.transpire.abc.abcnews.R

/**
 * This activity works as container of fragments
 */
class HomeNewsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_news_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, HomeNewsFragment.newInstance())
                .commitNow()
        }
    }

}

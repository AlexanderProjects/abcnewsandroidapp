package com.transpire.abc.abcnews.ui.Adapters.viewholders

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.View
import com.squareup.picasso.Picasso
import com.transpire.abc.abcnews.domain.models.Item
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    @SuppressLint("NewApi")
    private var outputFormat: DateFormat = SimpleDateFormat("MMM d, yyyy  hh:mm aaa", Locale.forLanguageTag("en-AU"))
    @SuppressLint("NewApi")
    private var inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.forLanguageTag("en-AU"))

    fun pubDateFormated(pubDate: String) : String = outputFormat.format(inputFormat.parse(pubDate))

    internal abstract fun bind(article: Item)
}
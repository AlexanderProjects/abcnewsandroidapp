package com.transpire.abc.abcnews.domain.usecases

import com.transpire.abc.abcnews.domain.models.NewsArticles
import com.transpire.abc.abcnews.domain.repository.FeedNewsRepository
import io.reactivex.Observable
import javax.inject.Inject

/**
 * This class process the usecase
 */
class NewsUseCase @Inject constructor (private val feedNewsRepository: FeedNewsRepository) : UseCase<NewsArticles>() {

    override fun buildUseCaseObservable(): Observable<NewsArticles> = feedNewsRepository.getNews()

}


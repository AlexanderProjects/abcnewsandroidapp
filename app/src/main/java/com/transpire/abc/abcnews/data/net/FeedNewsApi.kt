package com.transpire.abc.abcnews.data.net

import com.transpire.abc.abcnews.domain.models.NewsArticles
import io.reactivex.Flowable
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Url

/**
 * Executes the current use case.
 * @param useCaseSubscriber The guy who will be listen to the observable build with [ ][.buildUseCaseObservable].
 */

interface FeedNewsApi {
    @Headers("Content-Type: application/json")
    @GET
    fun feedNews(@Url url:String): Observable<NewsArticles>
}
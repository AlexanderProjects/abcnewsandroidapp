package com.transpire.abc.abcnews.di.modules

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.transpire.abc.abcnews.di.factory.ViewModelFactory
import com.transpire.abc.abcnews.di.scope.ViewModelKey
import com.transpire.abc.abcnews.ui.homenews.HomeNewsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeNewsViewModel::class)
    internal abstract fun bindHomeNewsViewModel(homeViewModel: HomeNewsViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

}
package com.transpire.abc.abcnews.di.components

import com.transpire.abc.abcnews.AbcNewsApplication
import com.transpire.abc.abcnews.di.modules.ApplicationModule
import com.transpire.abc.abcnews.di.modules.FragmentModule
import com.transpire.abc.abcnews.di.modules.NetModule
import com.transpire.abc.abcnews.di.modules.ViewModelModule
import com.transpire.abc.abcnews.ui.homenews.HomeNewsFragment
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = [AndroidInjectionModule::class, FragmentModule::class, ViewModelModule::class, ApplicationModule::class, NetModule::class])
interface ApplicationComponent {
    fun inject(abcNewsApplication: AbcNewsApplication)
    fun inject(homeNewsFragment: HomeNewsFragment)
}
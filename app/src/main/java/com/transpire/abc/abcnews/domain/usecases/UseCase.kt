package com.transpire.abc.abcnews.domain.usecases

import android.annotation.SuppressLint
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

abstract class UseCase<T> protected constructor() {

    private var disposable = CompositeDisposable()

    /**
     * Builds an [Observable] which will be used when executing the current [UseCase].
     */
    protected abstract fun buildUseCaseObservable(): Observable<T>

    /**
     * Executes the current use case.
     */

    @SuppressLint("CheckResult")
    fun execute(useCaseObserver: DisposableObserver<T>) = disposable.add(this.buildUseCaseObservable()
                                                                            .subscribeOn(Schedulers.newThread())
                                                                            .observeOn(AndroidSchedulers.mainThread())
                                                                            .subscribeWith(useCaseObserver))

    fun dispose(){
        disposable.dispose()
    }


}
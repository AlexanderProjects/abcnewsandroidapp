package com.transpire.abc.abcnews.domain.models

data class Enclosure (var link : String?,
                      var type: String?,
                      var thumbnail: String?)
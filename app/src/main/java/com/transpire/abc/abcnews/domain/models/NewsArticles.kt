package com.transpire.abc.abcnews.domain.models

data class NewsArticles(var status: String?,
                        var feed: Feed?,
                        var items: ArrayList<Item>)
package com.transpire.abc.abcnews.di.modules

import com.transpire.abc.abcnews.ui.homenews.HomeNewsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    internal abstract fun contributeHomeNewsFragment(): HomeNewsFragment


}
package com.transpire.abc.abcnews.data

import com.transpire.abc.abcnews.domain.models.NewsArticles
import com.transpire.abc.abcnews.domain.repository.FeedNewsRepository
import io.reactivex.Flowable
import io.reactivex.Observable
import javax.inject.Inject

/**
 * This class sent to the factory all the calls
 */
class FeedNewsDataRepository @Inject constructor (private val dataFactory : DataFactory) : FeedNewsRepository {

    override fun getNews(): Observable<NewsArticles> = dataFactory.createSourceData().obtainNewsFeed()

}
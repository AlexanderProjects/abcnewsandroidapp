package com.transpire.abc.abcnews.ui.homenews


import android.arch.lifecycle.ViewModel
import com.transpire.abc.abcnews.domain.models.NewsArticles
import com.transpire.abc.abcnews.domain.usecases.NewsUseCase
import io.reactivex.observers.DisposableObserver

import javax.inject.Inject

class HomeNewsViewModel @Inject constructor(private var newsUseCase: NewsUseCase) : ViewModel() {

    private lateinit var view: HomeNewsView

    fun getListOfNews(){
        newsUseCase.execute(createStringSubscriber())
    }

    fun injectView(view: HomeNewsView){
        this.view = view
    }


    private fun createStringSubscriber(): DisposableObserver<NewsArticles> {
        val subscriber = object: DisposableObserver<NewsArticles>() {
            override fun onNext(newsArticles: NewsArticles) {
               view.setAdapter(newsArticles.items)
            }

            override fun onComplete() {
                view.stopRefresh()
            }

            override fun onError(e: Throwable) {
                println("onError")
            }

        }

        return subscriber
    }

    override fun onCleared() {
        super.onCleared()
        newsUseCase.dispose()
    }
}

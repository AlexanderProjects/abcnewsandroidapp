package com.transpire.abc.abcnews.domain.repository

import com.transpire.abc.abcnews.domain.models.NewsArticles
import io.reactivex.Observable

interface FeedNewsRepository {
    fun getNews() : Observable<NewsArticles>
}